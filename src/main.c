#include "mem.h"
#include <stdio.h>

#define INITIAL_HEAP_SIZE 0
#define DEFAULT_HEAP_SIZE 8192
#ifndef MAP_ANONYMOUS
    #define MAP_ANONYMOUS 0x20
#endif

void test_init_and_free() {
    printf("Test init\n");
    void* heap = heap_init(INITIAL_HEAP_SIZE);
    void* block_1 = _malloc(1); //try small block
    debug_heap(stdout, heap);
    void* block_2 = _malloc(1000); //try big block
    debug_heap(stdout, heap);
    void* block_3 = _malloc(10000); //try very big block
    debug_heap(stdout, heap);

    printf("Test free\n");
    _free(block_1);
    debug_heap(stdout, heap);
    _free(block_2);
    debug_heap(stdout, heap);
    _free(block_3);
    debug_heap(stdout, heap);

    heap_term();
}

void test_free_middle_block() {
    printf("Test free middle block\n");
    void* heap = heap_init(INITIAL_HEAP_SIZE);
    _malloc(100);
    void* block_1 = _malloc(100);
    void* block_2 = _malloc(100);
    void* block_3 = _malloc(100);
    _malloc(100);
    debug_heap(stdout, heap);
    
    _free(block_1);
    _free(block_3);
    debug_heap(stdout, heap);
    _free(block_2);
    debug_heap(stdout, heap);

    heap_term();
}

void test_another_place_region() {
    printf("Test new region in another place\n");
    void* heap = heap_init(INITIAL_HEAP_SIZE);
    //allocate memory next to our heap
    void* some_memory = mmap(heap + DEFAULT_HEAP_SIZE, 1000,  PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS, -1, 0 );
    _malloc(5000); //block is big enough to occupy more than a half of place in heap
    _malloc(5000); //this block should in another part of memory
    debug_heap(stdout, heap);

    munmap(some_memory, 1000);
    heap_term();
}

int main() {
    test_init_and_free();
    test_free_middle_block();
    test_another_place_region();
    
    return 0;
}